﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene i = Inimene.New("35503070211");
        }
    }

    class Õpetaja : Inimene
    {
        public string Aine;

        protected Õpetaja(string ik) : base(ik) { }
        public static new Õpetaja New(string ik)
        {
            if (Kontrolli(ik)) return new Õpetaja(ik);
            else return null;
        }
    }
    class Õpilane : Inimene
    {
        public string Klass;

        protected Õpilane(string ik) : base(ik) { }
        public static new Õpilane New(string ik)
        {
            if (Kontrolli(ik)) return new Õpilane(ik);
            else return null;
        }
    }
    class Inimene
    {
        public readonly string IK;
        public string Nimi;

        protected Inimene(string ik)
        {
            IK = ik;
        }

        protected static bool Kontrolli (string ik)
        {
            return true;
        }

        public static  Inimene New(string ik)
        {
            bool kontrollitud = false;
            kontrollitud = Kontrolli(ik);
            // siin teen eelneva kontrolli

            if (kontrollitud) return new Inimene(ik);
            else return null;

        }
    }
}
