﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// kuskile oleks vaja teha nimekiri õpilasteswt ja õpetajatest ehk Inimestest
// paluti et see oleks Dictionary<isikukood,inimene>


namespace ConsoleApp2
{
    class Program
    {

        static void Main(string[] args)
        {
            Person p = new Person("35503070211")
            {
                Firstname = "Henn",
                Lastname = "Sarv",
            };

            string filename = @"..\..\õpetajad.txt";
            string[] loetudread = System.IO.File.ReadAllLines(filename);
            foreach(var loetudrida in loetudread)
            {
                var tükid = (loetudrida + ",,,").Split(',');
                new Teacher(tükid[0].Trim())
                {
                    Firstname = tükid[1].Trim(),
                    Lastname = tükid[2].Trim(),
                    Subject = tükid[3].Trim(),
                    Form = tükid[4].Trim()
                };
            }

            filename = @"..\..\õpilased.txt";
            loetudread = System.IO.File.ReadAllLines(filename);
            foreach (var loetudrida in loetudread)
            {
                var tükid = (loetudrida + ",,,").Split(',');
                new Pupil(tükid[0].Trim())
                {
                    Firstname = tükid[1].Trim(),
                    Lastname = tükid[2].Trim(),
                    
                    Form = tükid[3].Trim()
                };
                
            }
            foreach (var x in Person.KooliNimekiri) Console.WriteLine(x);
            Console.WriteLine("\nsiiamaani on kõik olemas vaateme nüüd sünnipäevi\n");

            foreach (var x in Person.KooliNimekiri.Values) Console.WriteLine($"{x.Firstname} on sündinud {x.BirthDay:dd.MM.yyyy} ja ta  on {x.Age} aastat vana");

            // me pidime leidma, kellel on järgmine sünnipäev
            // järgmine sünnipäev - kellel see iganes on, tuleb järgmise 366 päeva jooksul?
            // inimene saab oma järgtmisel sünnipäeval aasta vanemaks ?
            // inimese järgmine sünnipäeva on päeval Sünnikuupäev.AddYears(Vanus+1) ?

            DateTime next = DateTime.MaxValue; // no enne seda päeva kellelgi ikka sünnipäev tuleb
            string kes = "";
            foreach(var x in Person.KooliNimekiri)
                if (x.Value.BirthDay.AddYears(x.Value.Age + 1) < next)
                {
                    kes = x.Key;
                    next = x.Value.BirthDay.AddYears(x.Value.Age + 1);
                }

            Console.WriteLine($"järgmisena peab sünnipäeva {Person.KooliNimekiri[kes].Fullname}");

            // see leiab kõik, kel järgmisel sünnipäevapäeval on sünnipäev
            // seda oskad sa lugeda peale tänast juttu (kui siiski?)
            // selliseid võiks osata kirjutada (millalgi?)
            Person.KooliNimekiri.Values
                .Select(x => new { x.Fullname, x.Age, NextBirtDay = x.BirthDay.AddYears(x.Age + 1) })
                .ToLookup(x => x.NextBirtDay)
                .OrderBy(x => x.Key)
                .FirstOrDefault()
                .ToList()
                .ForEach(x => Console.WriteLine(x))
                ;


            
            
        }
    }

    // proovime siia sisse korrata, mis on mis ja kes on kes

    class Person
    {
        public static Dictionary<string, Person> KooliNimekiri = new Dictionary<string, Person>();

        public readonly string IK;
        public string Firstname = "";
        public string Lastname = "";
        public string Fullname => Firstname + " " + Lastname;
        //{
        //    get => Firstname + " " + Lastname;
        //    #region Polevaja
        //    //set
        //    //{
        //    //    Firstname = value.Split(' ')[0];
        //    //    Lastname = value.Split(' ')[1];
        //    //} 
        //    #endregion
        //}

        #region Mõttetud asjad
        //public string Firstname
        //{
        //    get => _Firstname;
        //    set
        //    {
        //        _Firstname = value;
        //        Fullname = _Firstname + " " + _Lastname;
        //    }
        //}
        //public string Lastname
        //{
        //    get => _Lastname;
        //    set
        //    {
        //        _Lastname = value;
        //        Fullname = _Firstname + " " + _Lastname;
        //    }
        //} 
        #endregion

        //public String IK
        //{
        //    get => _IK;
        //    set => _IK = _IK == "" ? value : _IK;
        //}

        //public void MuudaIK(string uusIk)
        //{
        //    _IK = uusIk;
        //}

        // mis asi on konstruktor?
        // meetod (voidi ei kirjutata)
        // nimi = klassinimi

        public Person(string ik)
        {
            this.IK = ik;
            KooliNimekiri.Add(this.IK, this);
        }     

        //public void AddToList()
        //{
        //    KooliNimekiri.Add(this.IK, this);
        //}

        // teeme siia funktsiooni IK -> Datetime
        public DateTime BirthDay
        {
            // see peaks returnima meile miskise datetime
            
            //int sajand = 1800;
            //if (IK.Substring(0, 1) == "3") sajand = 1900;
            //if (IK.Substring(0, 1) == "4") sajand = 1900;
            //if (IK.Substring(0, 1) == "5") sajand = 2000;
            //if (IK.Substring(0, 1) == "6") sajand = 2000;
            ////            if (IK[0] > '4') sajand = 2000;

            //sajand = IK.Substring(0, 1) == "3" ? 1900 :
            //         IK.Substring(0, 1) == "4" ? 1900 :
            //         IK.Substring(0, 1) == "5" ? 2000 :
            //         IK.Substring(0, 1) == "6" ? 2000 : 1800;
 

            get => new DateTime(
                //sajand +
                (IK.Substring(0, 1) == "3" ? 1900 :
                     IK.Substring(0, 1) == "4" ? 1900 :
                     IK.Substring(0, 1) == "5" ? 2000 :
                     IK.Substring(0, 1) == "6" ? 2000 : 1800) +

                int.Parse(IK.Substring(1, 2)), // siia tuleks saada aasta 
                int.Parse(IK.Substring(3, 2)),    // siia tuleks saada kuu
                int.Parse(IK.Substring(5, 2))    // siia tuleks saada päev - int.Parse(IK.Substring(5,2))
                );  
        }

        public int Age => (DateTime.Today - BirthDay).Days * 4 / 1461;

        public override string ToString()
        {
            return $"{Fullname} ({IK})";
        }
    }

    class Teacher : Person
    {
        public string Subject; // õpetaja on muidu täitsa tavaline inimene, aga tal on subject
        public string Form;    // mõni õpetaja on ka klassijuhataja

        public Teacher(string ik) : base(ik) { }

        public override string ToString() => $"{Subject} õpetaja {Fullname} {(Form == "" ? "" : $" {Form} klassi juhataja")} ({IK})";
    }
    class Pupil : Person
    {
        public string Form; // õpilane on muidu täitsa tavaline inimene, aga tal on koolivorm

        public Pupil(string ik) : base(ik) { }

        public override string ToString() => $"{Form} klassi õpilane {Fullname} ({IK})";
    }

}
